#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define false 0
#define true 1

typedef int bool;


typedef struct node_s node_t;

void parse(char* input);

char* read_line(void);

void print(node_t* head);

bool headEmpty(void);

bool headError(node_t* n);

extern node_t* headnode_ptr;

extern node_t* currnode_ptr;

extern node_t* headnodeVertical_ptr;

