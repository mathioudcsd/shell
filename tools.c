#include "tools.h"

typedef struct node_s{	//vertically
	char* str;
	struct node_s* next_ver;
	struct node_s* next_hor;
	struct node_s* prev_hor;

}node_t;

char* read_line(void){
	char* line=NULL;
	size_t buffsize=0;
	getline(&line, 0, stdin);
	return line;
}

bool headEmpty(void){
	if(headnode_ptr==NULL)
		return true;
	else
		return false;
}

bool headError(node_t* n){
	if(headnode_ptr==NULL){
		fprintf(stderr, "Command starts with illegal word/phrase. Could not execute.\n\n");
		return true;
	}
	if((n->prev_hor)==NULL){
		fprintf(stderr, "Command has illegal word/phrase after Pipe. Could not execute.\n\n");
		return true;
	}
	else{
		fprintf(stderr, "no head error\n");
		return false;
	}
}

void parse(char* input){

	char* pch;
	bool check=false;
	bool foundPipe=false;
	node_t* tmpnode;
	node_t* currnode;
	pch=strtok(input, "\n ");
	while(pch!=NULL){
		if((strcmp(pch, "<")==0) || (strcmp(pch, ">")==0) || (strcmp(pch, ">>")==0) || (strcmp(pch, "&")==0)) {
			// check = headError(currnode);
			// if(check)
			// 	return -1;
			tmpnode=(node_t*)malloc(sizeof(node_t));
			tmpnode->str=(char*)malloc(strlen(pch)+2);
			strcpy(tmpnode->str, pch);
			currnode->next_hor=tmpnode;
			tmpnode->next_hor=NULL;
			tmpnode->prev_hor=currnode;
			tmpnode->next_ver=NULL;
			currnode=tmpnode;
		}

		else if(strcmp(pch, "|")==0){
			// check = headError(currnode);
			// if(check)
			// 	return -1;
			foundPipe=true;
			tmpnode=(node_t*)malloc(sizeof(node_t));
			tmpnode->str=(char*)malloc(strlen(pch)+2); //overflow?
			strcpy(tmpnode->str, pch);
			currnode->next_hor=tmpnode;
			tmpnode->next_hor=NULL;
			tmpnode->prev_hor=currnode;
			tmpnode->next_ver=NULL;
			currnode=tmpnode;
		}

		else if((pch[0]=='-') && strlen(pch)>1){
			// check = headError(currnode);
			// if(check)
			// 	return -1;
			// PART 1
			tmpnode=(node_t*)malloc(sizeof(node_t));
			tmpnode->str=(char*)malloc(strlen(pch));	
			strncpy(tmpnode->str, pch, 1);
			currnode->next_hor=tmpnode;
			tmpnode->next_hor=NULL;
			tmpnode->prev_hor=currnode;
			tmpnode->next_ver=NULL;
			currnode=tmpnode;
			// PART 2
			tmpnode=(node_t*)malloc(sizeof(node_t));
			tmpnode->str=(char*)malloc(strlen(pch+1));
			strcpy(tmpnode->str, pch+1);
			currnode->next_hor=tmpnode;
			tmpnode->next_hor=NULL;
			tmpnode->prev_hor=currnode;
			tmpnode->next_ver=NULL;
			currnode=tmpnode;
		}

		else{
			tmpnode=(node_t*)malloc(sizeof(node_t));
			tmpnode->str=(char*)malloc(strlen(pch));
			strcpy(tmpnode->str, pch);
			// if(tmpnode->str[strlen(pch)-1]=='\n') {  //epilusi tou '/n' sto telos
			// 	fprintf(stdout, "vrika newline sto %s\n",pch );
			// 	tmpnode->str[strlen(pch)-1]='\0';
			// }
			//fprintf(stderr, "word: %s, size: %d\n", tmpnode->str, strlen(pch) );
			check=headEmpty();
			if(check){
				// fprintf(stdout, "head empty\n");
				tmpnode->next_hor=NULL;
				tmpnode->prev_hor=NULL;
				tmpnode->next_ver=NULL;
				headnodeVertical_ptr=tmpnode;
				headnode_ptr=tmpnode;
				currnode=tmpnode;
			}
			else{
				if(foundPipe==true){
					fprintf(stderr, "found pipe\n");
					tmpnode->next_hor=NULL;
					tmpnode->prev_hor=NULL;
					tmpnode->next_hor=NULL;
					headnodeVertical_ptr->next_ver=tmpnode;
					headnodeVertical_ptr=tmpnode;
					currnode=tmpnode;
					foundPipe=false;
				}
				else{
					currnode->next_hor=tmpnode;
					tmpnode->next_hor=NULL;
					tmpnode->prev_hor=currnode;
					tmpnode->next_ver=NULL;
					currnode=tmpnode;
					foundPipe=false;
				}
			}
		}
		pch=strtok(NULL, "\n ");
	}
}

void print(node_t* head){
	fprintf(stderr, "RESULTS:\n\n\n\n" );
	currnode_ptr=headnode_ptr;
	headnodeVertical_ptr=headnode_ptr;
	while(currnode_ptr!=NULL && headnodeVertical_ptr!=NULL ){
		fprintf(stdout, "%s\t\t\t", currnode_ptr->str);
		//sleep(1);
		currnode_ptr=currnode_ptr->next_hor;
		if(currnode_ptr==NULL){
			fprintf(stdout, "\n\n\n");
			headnodeVertical_ptr=headnodeVertical_ptr->next_ver;
			if(headnodeVertical_ptr!=NULL){
				currnode_ptr=headnodeVertical_ptr;
				//fprintf(stderr, "katevike to currnode\n");
				continue;
			}
			else
				//fprintf(stderr, "vert=null\n");
				return;
		}
		// fprintf(stderr, "i'm inhere!\n");
	} 
}